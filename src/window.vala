/* window.vala
 *
 * Copyright 2023 Link Dupont
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

namespace Settings {
    [GtkTemplate (ui = "/app/drey/settings/window.ui")]
    public class Window : Adw.ApplicationWindow {
        [GtkChild] private unowned Gtk.ListBox schemas;
        [GtkChild] private unowned Adw.NavigationPage content_page;
        [GtkChild] private unowned Adw.Bin schema_content;

        private Gtk.StringList list_model;

        public Window (Gtk.Application app) {
            Object (application: app);
        }

        construct {
            var source = GLib.SettingsSchemaSource.get_default ();
            string[] non_relocatable;
            string[] relocatable;
            source.list_schemas (true, out non_relocatable, out relocatable);

            this.list_model = new Gtk.StringList (non_relocatable);

            this.schemas.bind_model (this.list_model, (item) => {
                var e = item as Gtk.StringObject;
                return new SidebarRow () {
                    title = e.string,
                };
            });
        }

        [GtkCallback]
        private void on_list_activated (Gtk.ListBox box, Gtk.ListBoxRow row) {
            if (row != null) {
                var sidebar_row = row as SidebarRow;
                this.content_page.title = sidebar_row.title;

                Adw.PreferencesGroup pref_group = new Adw.PreferencesGroup ();

                GLib.SettingsSchemaSource source = GLib.SettingsSchemaSource.get_default ();
                GLib.SettingsSchema schema = source.lookup (sidebar_row.title, true);
                GLib.Settings settings = new GLib.Settings (sidebar_row.title);
                string[] keys = schema.list_keys ();
                for (int i = 0; i < keys.length; i++) {
                    GLib.SettingsSchemaKey key = schema.get_key (keys[i]);
                    GLib.VariantType value_type = key.get_value_type ();
                    switch (value_type.dup_string ()) {
                    case "d":
                        Adw.EntryRow pref_row = new Adw.EntryRow ();
                        pref_row.title = key.get_name ();
                        pref_row.text = "%l".printf(settings.get_double (key.get_name ()));
                        pref_group.add (pref_row);
                        break;
                    case "b":
                        Adw.SwitchRow pref_row = new Adw.SwitchRow ();
                        pref_row.title = key.get_name ();
                        pref_row.active = settings.get_boolean (key.get_name ());
                        pref_group.add (pref_row);
                        break;
                    case "s":
                        Adw.EntryRow pref_row = new Adw.EntryRow ();
                        pref_row.title = key.get_name ();
                        pref_row.text = settings.get_string (key.get_name ());
                        pref_group.add (pref_row);
                        break;
                    default:
                        info (@"unsupported type '$(value_type.dup_string())' for key $(sidebar_row.title).$(key.get_name ())");
                        break;
                    }
                }
                Adw.PreferencesPage pref_page = new Adw.PreferencesPage ();
                pref_page.add (pref_group);

                this.schema_content.child = pref_page;
            }
        }
    }

    private class SidebarRow : Gtk.ListBoxRow {
        public string title { get; set; }

        construct {
            var hbox = new Gtk.Box (Gtk.Orientation.HORIZONTAL, 12) {
                margin_top = 12,
                margin_bottom = 12,
                margin_start = 6,
                margin_end = 6,
            };

            var label = new Gtk.Label ("") {
                halign = Gtk.Align.START,
            };
            this.bind_property ("title", label, "label", GLib.BindingFlags.DEFAULT | GLib.BindingFlags.SYNC_CREATE);
            hbox.append (label);
            this.set_child (hbox);
        }

        ~SidebarRow() {
            debug (@"[%p] ~$(this.get_type().name())", this);
        }
    }
}
